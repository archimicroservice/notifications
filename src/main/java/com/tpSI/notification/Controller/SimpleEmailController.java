package com.tpSI.notification.Controller;

//import jakarta.ws.rs.GET;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

//
@CrossOrigin("*")
@RestController
@RequestMapping("mail")
public class SimpleEmailController {

        @Value("${spring.mail.username}")
        private String  mailUsername;
        @Autowired
        private JavaMailSender mailSender;
        @GetMapping("send/{mail}")
        public void sendEmail(@PathVariable("mail") String toEmail,
                              @RequestParam("body") String body,
                              @RequestParam("subject") String subject){

            try {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setFrom(mailUsername);
                message.setTo(toEmail);
                message.setText(body);
                message.setSubject(subject);

                mailSender.send(message);
                System.out.println("Email envoyé");
            } catch (Exception e) {
                // Gérer l'exception ici
                System.err.println("Erreur lors de l'envoi de l'e-mail : " + e.getMessage());
            }
        }

    public JavaMailSender emailSender;




}

